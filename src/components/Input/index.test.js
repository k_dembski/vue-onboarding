/* eslint-disable no-undef */
import { mount, createLocalVue } from '@vue/test-utils';
import component from './index';
import { ValidationProvider } from 'vee-validate';
import flushPromises from 'flush-promises';

describe('Input', () => {
  let wrapper;
  const localVue = createLocalVue();
  localVue.component('ValidationProvider', ValidationProvider);

  beforeEach(() => {
    wrapper = mount(component, { localVue, propsData: { id: 'customId' } });
  });

  it('should emit value to parent component on input event', async () => {
    const input = wrapper.find('input');
    input.element.value = 'custom value';
    await input.trigger('input');
    expect(wrapper.emitted().input[0]).toEqual(['custom value']);
  });

  it('should render error if input value is empty', async () => {
    const input = wrapper.find('input');
    expect(wrapper.find('svg').exists()).toBe(false);

    input.element.value = 'test value';
    await input.trigger('input');
    input.element.value = '';
    await input.trigger('input');
    await flushPromises();

    expect(wrapper.find('svg').exists()).toBe(true);
  });
});
