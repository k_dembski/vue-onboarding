/* eslint-disable no-undef */
import { mount } from '@vue/test-utils';
import component from './index';

describe('Button', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(component, { propsData: { id: 'customId' } });
  });

  it('should render stretched variant classes based on stretched prop', async () => {
    await wrapper.setProps({ stretched: true });
    expect(wrapper.classes()).toContain('w-full');
  });

  it('should render variant classes based on variant prop', async () => {
    expect(wrapper.classes()).toEqual(expect.arrayContaining(['bg-primary-blue', 'text-white']));

    await wrapper.setProps({ variant: 'secondary' });
    expect(wrapper.classes()).toEqual(expect.arrayContaining(['bg-white', 'text-primary-blue']));
  });

  it('should render size classes based on size prop', async () => {
    expect(wrapper.classes()).toEqual(expect.arrayContaining(['py-2.5', 'px-6', 'rounded']));

    await wrapper.setProps({ size: 'small' });
    expect(wrapper.classes()).toEqual(expect.arrayContaining(['py-1', 'px-4', 'rounded-xs']));

    await wrapper.setProps({ size: 'large' });
    expect(wrapper.classes()).toEqual(expect.arrayContaining(['py-3.5', 'px-8', 'rounded-md']));
  });
});
